function install(files, aliases)
	print("Creating directories/removing old files...")
	for file, url in pairs(files) do
		local f = fs.open(file, "w")
		f.close()
		fs.delete(file)
	end

	print("Downloading files...")
	for file, url in pairs(files) do
		shell.run("wget", url, file)
	end
	
	for alias, file in pairs(aliases) do
		shell.run("alias", alias, file)
	end
end

-- Get command params
args = { ... }

local files = {}
local aliases = {}

local repo = "https://bitbucket.org/crgibson/turtles/raw/master"

if args[1] == "miner" then
	local program = "/gibsdev/miner/miner.lua"
	files[program] = repo .. "/miner/miner.lua"
	files["/gibsdev/api/arguments.lua"] = repo .. "/api/arguments.lua"
	files["/gibsdev/api/coords.lua"] = repo .. "/api/coords.lua"
	files["/gibsdev/api/utils.lua"] = repo .. "/api/utils.lua"

	aliases["miner"] = program
	install(files, aliases)
elseif args[1] == "sorter" then
	local program = "/gibsdev/sorter/sorter.lua"
	files[program] = repo .. "/sorter/sorter.lua"
	files["/gibsdev/api/arguments.lua"] = repo .. "/api/arguments.lua"
	files["/gibsdev/api/coords.lua"] = repo .. "/api/coords.lua"
	files["/gibsdev/api/utils.lua"] = repo .. "/api/utils.lua"

	aliases["sorter"] = program
	install(files, aliases)
else
	error("Invalid program name")
end

